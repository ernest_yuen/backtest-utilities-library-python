from setuptools import setup, find_packages

version = {}
with open("networkandutilitiespy/version.py") as file:
    exec(file.read(), version)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="networkandutilitiespy",
    version=version['__version__'],
    author="Ernest Yuen",
    author_email="ernestyuen08@gmail.com",
    description="Backtest Utilities Library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ernest_yuen/backtest-utilities-library-python",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
