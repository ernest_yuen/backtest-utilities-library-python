import matplotlib.pyplot as plt
import numpy as np
from matplotlib.dates import DateFormatter, WeekdayLocator, DayLocator, MONDAY, date2num
from .mpl_finance import *


def candlePlot(series_data, title='a'):
    Date = [date2num(date) for date in series_data.index]
    series_data.loc[:, 'Date'] = Date

    list_data = []
    for i in range(len(series_data)):
        a = [series_data.Date[i], series_data.Open[i], series_data.High[i], series_data.Low[i], series_data.Close[i]]
        list_data.append(a)

    ax = plt.subplot()
    mondays = WeekdayLocator(MONDAY)
    DayLocator.MAXTICKS = 100000000000000000
    weekFormatter = DateFormatter('%y %b %d')
    ax.xaxis.set_major_locator(mondays)
    ax.xaxis.set_minor_locator(DayLocator())
    ax.xaxis.set_major_formatter(weekFormatter)

    candlestick_ohlc(ax, list_data, width=0.7, colorup='g', colordown='r')
    ax.set_title(title)
    plt.setp(plt.gca().get_xticklabels(), rotation=50, horizontalalignment='center')

    return plt.show()


def candleLinePlots(candleData, candleTitle='a', **kwargs):
    Date = [date2num(date) for date in candleData.index]
    candleData.loc[:, 'Date'] = Date

    list_data = []
    for i in range(len(candleData)):
        a = [candleData.Date[i], candleData.Open[i], candleData.High[i], candleData.Low[i], candleData.Close[i]]
        list_data.append(a)

    ax = plt.subplot()

    flag = 0
    if kwargs:
        if kwargs['splitFigures']:
            ax = plt.subplot(211)
            ax2 = plt.subplot(212)
            flag = 1

    for key in kwargs:
        if key == 'title':
            ax2.set_title(kwargs[key])
        if key == 'ylabel':
            ax2.set_ylabel(kwargs[key])
        if key == 'grid':
            ax2.grid(kwargs[key])
        if key == 'Data':
            plt.sca(ax)
            if flag:
                plt.sca(ax2)

            if kwargs[key].ndim == 1:
                plt.plot(kwargs[key], color='k', label=kwargs[key].name)
                plt.legend(loc='best')
            elif all([kwargs[key].ndim == 2, len(kwargs[key].columns) == 2]):
                plt.plot(kwargs[key].iloc[:, 0], color='k', label=kwargs[key].iloc[:, 0].name)
                plt.plot(kwargs[key].iloc[:, 1], linestyle='dashed', label=kwargs[key].iloc[:, 1].name)
                plt.legend(loc='best')
            elif all([kwargs[key].ndim == 2, len(kwargs[key].columns) == 3]):
                plt.plot(kwargs[key].iloc[:, 0], color='k', label=kwargs[key].iloc[:, 0].name)
                plt.plot(kwargs[key].iloc[:, 1], linestyle='dashed', label=kwargs[key].iloc[:, 1].name)
                plt.bar(x=kwargs[key].iloc[:, 2].index, height=kwargs[key].iloc[:, 2],
                        color='r', label=kwargs[key].iloc[:, 2].name)
                plt.legend(loc='best')

    mondays = WeekdayLocator(MONDAY)
    DayLocator.MAXTICKS = 100000000000000000
    weekFormatter = DateFormatter('%y %b %d')
    ax.xaxis.set_major_locator(mondays)
    ax.xaxis.set_minor_locator(DayLocator())
    ax.xaxis.set_major_formatter(weekFormatter)
    plt.sca(ax)
    candlestick_ohlc(ax, list_data, width=0.7, colorup='g', colordown='r')
    ax.set_title(candleTitle)
    plt.setp(plt.gca().get_xticklabels(), rotation=20, horizontalalignment='center')
    ax.autoscale_view()

    return plt.show()


def candleVolume(series_data, candletitle='a', bartitle=''):
    Date = [date2num(date) for date in series_data.index]
    series_data.index = list(range(len(Date)))
    series_data['Date'] = Date
    listData = zip(series_data.Date, series_data.Open, series_data.High, series_data.Low, series_data.Close)
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212)
    for ax in ax1, ax2:
        mondays = WeekdayLocator(MONDAY)
        weekFormatter = DateFormatter("%m/%d/%Y")
        ax.xaxis.set_major_locator(mondays)
        ax.xaxis.set_minor_locator(DayLocator())
        ax.xaxis.set_major_formatter(weekFormatter)
        ax.grid(True)

    ax1.set_ylim(series_data.Low.min()-2, series_data.High.max()+2)
    ax1.set_ylabel('Candlestick Charts & Close Price')
    candlestick_ohlc(ax1, listData, width=0.7, colorup='g', colordown='r')
    plt.setp(plt.gca().get_xticklabels(), rotation=20, horizontalalignment='center')
    ax1.autoscale_view()
    ax1.set_title(candletitle)
    ax1.plot(series_data.Date, series_data.Close, color='black', label='Close Price')
    ax1.legend(loc='best')
    ax2.set_ylabel('Volume')
    # ax2.set_ylim(0, series_data.Volume.max()/5)
    ax2.set_ylim(0, series_data.Volume.max()*3)

    ax2.bar(np.array(Date)[np.array(series_data.Close >= series_data.Open)],
            height=series_data.iloc[:, 4][np.array(series_data.Close >= series_data.Open)], color='g', align='center')
    ax2.bar(np.array(Date)[np.array(series_data.Close < series_data.Open)],
            height=series_data.iloc[:, 4][np.array(series_data.Close < series_data.Open)], color='r', align='center')
    ax2.set_title(bartitle)
    return plt.show()

